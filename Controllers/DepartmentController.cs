namespace Task.Controllers
{
    public class DepartmentController :Controllers
    {
         private readonly TaskDbContext _context;
        public DepartmentController(TaskDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _context.Depatments.ToListAsync());
        }
         public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskModel = await _context.Departments
                .FirstOrDefaultAsync(m => m.DeptCode == id);
            if (taskModel == null)
            {
                return NotFound();
            }

            return View(taskModel);
        }
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var departmentModel = await _context.Departments.FindAsync(id);
            if (departmentModel == null)
            {
                return NotFound();
            }
            return View(departmentModel);
        }

        
    }
}