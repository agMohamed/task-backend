﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Task.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }

        [Column("FullName", TypeName = "varchar")]
        [StringLength(100)]
        public string FullName { get; set; }

        [Column("NIC", TypeName = "varchar")]
        [StringLength(12)]
        [Required]
        public string NIC { get; set; }

        [Column("Imagename", TypeName = "varchar")]
        [StringLength(50)]
        public string Imagename { get; set; }

        [NotMapped]
        [DisplayName("UploadImage")]
        public IFormFile ImageFile { get; set; }

        [Column("Password", TypeName = "varchar")]
        [StringLength(12)]
        public string Password { get; set; }

        [Column("CurrentDeptCode", TypeName = "varchar")]
        [StringLength(20)]
        public string CurrentDeptCode { get; set; }
        public Department CurrentDepartment { get; set; }
    }
}
