﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task.Models
{
    public class TaskDbContext:DbContext
    {
        public TaskDbContext(DbContextOptions<TaskDbContext>options):base(options)
        {

        }
        public DbSet<Employee>Employees{ get; set; }
        public DbSet<Department>Departments{ get; set; }
    }
}
