﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Task.Models
{
    public class Department
    {
        [Key]
        [Column("DeptCode", TypeName = "varchar")]
        [StringLength(10)]
        public string DeptCode { get; set; }

        [Column("DeptName", TypeName = "varchar")]
        [StringLength(100)]
        [Required]
        public string DeptName { get; set; }

        [Column("DeptLocation", TypeName = "varchar")]
        [StringLength(100)]
        public string DeptLocation { get; set; }

        public ICollection<Employee> Employee { get; set; }
    }
}
