﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Task.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    DeptCode = table.Column<string>(type: "varchar", maxLength: 10, nullable: false),
                    DeptName = table.Column<string>(type: "varchar", maxLength: 100, nullable: false),
                    DeptLocation = table.Column<string>(type: "varchar", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.DeptCode);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FullName = table.Column<string>(type: "varchar", maxLength: 100, nullable: true),
                    NIC = table.Column<string>(type: "varchar", maxLength: 12, nullable: false),
                    Imagename = table.Column<string>(type: "varchar", maxLength: 50, nullable: true),
                    Password = table.Column<string>(type: "varchar", maxLength: 12, nullable: true),
                    CurrentDeptCode = table.Column<string>(type: "varchar", maxLength: 20, nullable: true),
                    CurrentDepartmentDeptCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                    table.ForeignKey(
                        name: "FK_Employees_Departments_CurrentDepartmentDeptCode",
                        column: x => x.CurrentDepartmentDeptCode,
                        principalTable: "Departments",
                        principalColumn: "DeptCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employees_CurrentDepartmentDeptCode",
                table: "Employees",
                column: "CurrentDepartmentDeptCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Departments");
        }
    }
}
